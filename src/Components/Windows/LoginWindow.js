import React, {Component} from 'react';
import {Card, CardTitle, CardText, CardActions} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Axios from '../../lib/axios';

class LoginWindow extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
    };

    this.handleUsernameInput = this.handleUsernameInput.bind(this);
    this.handlePasswordInput = this.handlePasswordInput.bind(this);

    this.handleSignIn = this.handleSignIn.bind(this);
    this.handleSignUp = this.handleSignUp.bind(this);

    this.userLoggedIn = this.userLoggedIn.bind(this);
    this.userSignedUp = this.userSignedUp.bind(this);

    this.signInError = this.signInError.bind(this);
    this.signUpError = this.signUpError.bind(this);

    this.ax = Axios();
  }

  handleUsernameInput(e, val) {
    this.setState({username: val});
  }

  handlePasswordInput(e, val) {
    this.setState({password: val});
  }

  userLoggedIn(data) {
    this.props.userCallback('signin-success', data);

    if (data !== null && data !== undefined) {
      let message = "Přihlášení uživatele '" + data.userName + "' proběhlo úspěšně.";
      this.props.snackBarCallback(message)
    } else {  // shouldn't appear.. fixed on server... removing on next commit
      this.signInError("userLoggedIn() called with null data.");
    }
  }

  userSignedUp(data) {
    this.props.userCallback('signup-success', data);

    if (data !== null && data !== undefined) {
      let message = "Registrace a přihlášení uživatele '" + data.userName + "' proběhlo úspěšně.";
      this.props.snackBarCallback(message)
    } else {
      this.signUpError("userSignedUp() called with null data.");
    }
  }

  signInError(error) {
    this.props.userCallback('signin-error', error);

    let message = "Vyskytla se chyba. Zkontrolujte konzoli pro případná chybová hlášení.";
    console.error(error);
    this.props.snackBarCallback(message)
  }

  signUpError(error) {
    this.props.userCallback('signup-error', error);

    let message = "Vyskytla se chyba při registraci. Zkontrolujte konzoli pro případná chybová hlášení.";
    console.error(error);

    this.props.snackBarCallback(message)
  }

  handleSignIn(e) {
    this.ax.post('/auth', {
      username: this.state.username,
      password: this.state.password
    }).then(response => {
      if (response !== null) {
        this.userLoggedIn(response.data);
      }
    }).catch(error => {
      this.signInError(error);
    })
  }

  handleSignUp(e) {
    this.ax.post('/user/new', {
      user: {
        userName: this.state.username,
        password: this.state.password
      }
    }).then(response => {
      if (response.status === 200) {
        this.userSignedUp(response.data)
      }
    }).catch(this.signUpError)
  }

  render() {
    return (
      <Card>
        <CardTitle title="Přihlašte se prosím"/>
        <CardText>
          <TextField name="username" hintText="např. johnDoe1337" floatingLabelText="Uživatelské jméno"
                     onChange={this.handleUsernameInput}/>
          <br/>
          <TextField name="password" hintText="vaše tajné heslo" floatingLabelText="Heslo" type="password"
                     onChange={this.handlePasswordInput}/>
        </CardText>
        <CardActions>
          <div className="row">
            <div className="col-md-6">
              <RaisedButton label="Přihlásit se" onClick={this.handleSignIn}/>
            </div>
            <div className="col-md-6">
              <RaisedButton label="Zaregistrovat se" onClick={this.handleSignUp}/>
            </div>
          </div>
        </CardActions>
      </Card>
    )
  }
}

export default LoginWindow;