import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import Axios from '../../lib/axios';
import {
  Table,
  TableBody,
  TableFooter,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';

class ClientsWindow extends Paper {
  constructor(props) {
    super(props);

    this.state = {
      clients: []
    };

    this.ax = Axios();

    this.handleSignOut = this.handleSignOut.bind(this)
  }

  handleSignOut() {
    this.props.userCallback('signout-success', {});
    this.props.snackBarCallback('Odhlášení proběhlo bez problémů!')
  }

  componentDidMount() {
    this.ax.get('/client/list').then(response => {
      if (response !== null) {
        if (response.data !== null && response.data !== undefined) {
          this.setState({clients: response.data});
        }
      }
    }).catch(error => {
      let message = "Vyskytla se chyba při načítání seznamu klientů.";
      this.props.snackBarCallback(message);
    })
  }

  render() {
    return (
      <div>
        <div>
          <Table
            height="300"
            fixedHeader={true}
            fixedFooter={true}
            selectable={false}
            multiSelectable={false}
          >
            <TableHeader
              displaySelectAll={false}
              adjustForCheckbox={false}
              enableSelectAll={false}
            >
              <TableRow>
                <TableHeaderColumn
                  colSpan={3}
                  tooltip="Přehled klientů"
                  style={{textAlign: 'center'}}
                >
                  Přehled klientů
                </TableHeaderColumn>
              </TableRow>
              <TableRow>
                <TableHeaderColumn tooltip="Identifikační kód klienta">ID</TableHeaderColumn>
                <TableHeaderColumn tooltip="Křestní jméno klienta">Křestní jméno</TableHeaderColumn>
                <TableHeaderColumn tooltip="Příjmení klienta">Přijmení</TableHeaderColumn>
                <TableHeaderColumn tooltip="Datum kdy byl klient v databázi vytvořen.">Datum stvoření</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody
              displayRowCheckbox={false}
              deselectOnClickaway={false}
              showRowHover={true}
              stripedRows={false}
            >
              {this.state.clients.map((row, index) => (
                <TableRow key={index}>
                  <TableRowColumn>{row._id}</TableRowColumn>
                  <TableRowColumn>{row.firstName}</TableRowColumn>
                  <TableRowColumn>{row.lastName}</TableRowColumn>
                  <TableRowColumn>{row.createdOn}</TableRowColumn>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </div>
        <RaisedButton label="Odhlásit se" onClick={this.handleSignOut}/>
      </div>
    )
  }
}

export default ClientsWindow