import React, {Component} from 'react';

import cookie from 'react-cookies';

import AppBar from 'material-ui/AppBar';
import Snackbar from 'material-ui/Snackbar';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Drawer from 'material-ui/Drawer';

import LoginWindow from './Windows/LoginWindow';
import MainWindow from './Windows/MainWindow';
import ClientsWindow from './Windows/ClientsWindow';
import ProjectsWindow from './Windows/ProjectsWindow';
import TasksWindow from './Windows/TasksWindow';

class WindowLayout extends Component {

  windowRouting = {
    "dashboard": this.drawDashboard,
    "clients": this.drawClients,
    "projects": this.drawProjects,
    "tasks": this.drawTasks
  };

  constructor(props) {
    super(props);

    this.handleAppBarClick = this.handleAppBarClick.bind(this);
    this.snackBarCallback = this.snackBarCallback.bind(this);

    this.userCallback = this.userCallback.bind(this);

    this.handleSnackbarCloseRequest = this.handleSnackbarCloseRequest.bind(this);

    this.loginRouter = this.loginRouter.bind(this);

    this.drawDashboard = this.drawDashboard.bind(this);
    this.drawClients = this.drawClients.bind(this);
    this.drawProjects = this.drawProjects.bind(this);
    this.drawTasks = this.drawTasks.bind(this);

    this.appBarDrawer = this.appBarDrawer.bind(this);

    this.handleDrawerRequestChange = this.handleDrawerRequestChange.bind(this);
  }

  componentWillMount() {
    let stateCookie = cookie.load('windowLayoutState');
    if (stateCookie === undefined) {
      this.setState({
        loggedUser: null,
        snackBar: {
          message: "",
          open: false
        },
        showDrawer: false,
        route: "dashboard"
      });
    } else {
      this.setState(stateCookie);
    }
  }

  handleAppBarClick(event) {
    if (this.state.loggedUser === null) {
      this.snackBarCallback("Menu je dostupné pouze přihlášeným uživatelům.")
    } else {
      this.setState({
        showDrawer: !this.state.showDrawer
      });
    }
  }

  snackBarCallback(message) {
    this.setState({
      snackBar: {
        message: message,
        open: true
      }
    })
  }

  handleSnackbarCloseRequest() {
    this.setState({
      snackBar: {
        message: "",
        open: false
      }
    })
  }

  userCallback(event, data) {
    console.log(event);
    switch (event) {
      case 'signup-success':
      case 'signin-success':
        this.setState({loggedUser: data});
        cookie.save('windowLayoutState', this.state);
        break;
      case 'signin-error':
      case 'signout-success':
      case 'signup-error':
        this.setState({loggedUser: null});
        cookie.remove('windowLayoutState');
        break;
      default:
        break;
    }
  }

  loginRouter() {
    if (this.state.loggedUser === null) {
      return (
        <LoginWindow
          snackBarCallback={this.snackBarCallback}
          userCallback={this.userCallback}/>
      )
    }
    else {
      return this.windowRouting[this.state.route](this.state.loggedUser);
    }
  }

  drawDashboard(loggedUser) {
    return (
      <MainWindow
        snackBarCallback={this.snackBarCallback}
        userCallback={this.userCallback}
        loggedUser={loggedUser}/>
    )
  }

  drawClients(loggedUser) {
    return (
      <ClientsWindow
        snackBarCallback={this.snackBarCallback}
        userCallback={this.userCallback}
        loggedUser={loggedUser}/>
    )
  }

  drawProjects(loggedUser) {
    return (
      <ProjectsWindow
        snackBarCallback={this.snackBarCallback}
        userCallback={this.userCallback}
        loggedUser={loggedUser}/>
    )
  }

  drawTasks(loggedUser) {
    return (
      <TasksWindow
        snackBarCallback={this.snackBarCallback}
        userCallback={this.userCallback}
        loggedUser={loggedUser}/>
    )
  }

  appBarDrawer() {
    if (this.state.loggedUser !== null) {
      return (
        <Drawer open={this.state.showDrawer} onRequestChange={(open) => this.setState({open})}>
          <Menu value={this.state.route} onChange={this.handleDrawerRequestChange}>
            <MenuItem value={'dashboard'}>Nástěnka</MenuItem>
            <MenuItem value={'clients'}>Klienti</MenuItem>
            <MenuItem value={'projects'}>Projekty</MenuItem>
            <MenuItem value={'tasks'}>Úkoly</MenuItem>
          </Menu>
        </Drawer>
      );
    }
  }

  handleDrawerRequestChange(event, value) {
    this.setState({showDrawer: false, route: value});
  }

  render() {
    return (
      <div className="container">
        <AppBar title="Souris DevNet" onLeftIconButtonClick={this.handleAppBarClick}/>
        {this.appBarDrawer()}
        {this.loginRouter()}
        <Snackbar
          open={this.state.snackBar.open}
          message={this.state.snackBar.message}
          autoHideDuration={3000}
          onRequestClose={this.handleSnackbarCloseRequest}/>
      </div>
    );
  }
}

export default WindowLayout;